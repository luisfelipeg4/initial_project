<?php

return [
    'name' => 'productos|producto',
    'title' => 'Productos',
    'columns' => [
        'actions' => 'Acciones',
        'name' => 'Nombre',
        'description' => 'Descripción',
        'photo' => 'Foto',
        'currency' => 'Moneda',
        'category' => 'Categoria',
        'price' => 'Price',
        'notFound' => 'No se han encontrado registros',
    ],
];
