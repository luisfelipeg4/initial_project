<?php

return [
    'name' => 'products|product',
    'title' => 'Products',
    'columns' => [
        'actions' => 'Actions',
        'name' => 'Name',
        'description' => 'Description',
        'photo' => 'Photo',
        'currency' => 'Currency',
        'category' => 'Category',
        'price' => 'Price',
        'notFound' => 'No records found',
    ],
];
