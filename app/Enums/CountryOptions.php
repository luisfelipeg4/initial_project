<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static COLOMBIA()
 * @method static static ECUADOR()
 */
final class CountryOptions extends Enum
{
    const COLOMBIA = 'co';
    const ECUADOR = 'ec';
}
